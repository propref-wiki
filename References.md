List of references of things we use during the development of PropRef

## Programming ##

  1. [GccGo Compilation](http://gcc.gnu.org/bugzilla/show_bug.cgi?id=57194)
  1. [Reading Files](https://gobyexample.com/reading-files)
  1. [Scaping \ ](https://github.com/StefanSchroeder/Golang-Regex-Tutorial/blob/master/01-chapter1.markdown)
  1. [Book of Miek](https://github.com/miekg/gobook) [[2](http://miek.nl/downloads/Go/)]
  1. [Packages](http://www.golang-book.com/11#section2) [[2](book of Miek#)]
  1. [Regular Expressions in Single Pass](https://groups.google.com/forum/#!topic/golang-nuts/vDRFh6ayjm8)
  1. [Errors in Go](http://golang.org/pkg/errors/)
  1. [GoBlog](https://github.com/icub3d/goblog) for understanding how to create a package
  1. [Returning Tuples without Parentesis](http://golangtutorials.blogspot.com.es/2011/06/return-values-from-go-functions.html)
  1. `file` version 5.18 detects properly ConTeXt files. Previous versions mismatch them with LaTeX files.
  1. [Iterating over Keys in Maps](http://stackoverflow.com/questions/1841443/iterating-over-all-the-keys-of-a-golang-map)
  1. [re2 Library Syntax](https://code.google.com/p/re2/wiki/Syntax) used in golang [regexp](http://golang.org/pkg/regexp/)
  1. [Stefan Schroeder Golang Regex Tutorial](https://github.com/StefanSchroeder/Golang-Regex-Tutorial) for learning syntax
  1. [regexpplanet](http://www.regexplanet.com/advanced/golang/index.html) for testing regexps in golang faster. It does not work for me
  1. [StackOverflow Question](http://stackoverflow.com/questions/6770898/unknown-escape-sequence-error-in-go) for knowing how to scape `[`
  1. [regoio](http://regoio.herokuapp.com) for testing regexps in golang faster. It works for me. Very good.

## Science publishing ##

  1. [Analysis of scientific publishing](http://news.sciencemag.org/scientific-community/2014/07/1-scientific-publishing)