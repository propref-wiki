The status is **undone**, but started (in *pre-alpha*)

For LaTeX source there are basically two possibilities: analyze directly the code or transform the code to other formats (mainly XML). There are several alternatives [here](http://jblevins.org/log/xml-tools): [LaTeXML](http://dlmf.nist.gov/LaTeXML/), [Tralics](http://www-sop.inria.fr/marelle/tralics/), [Hermes](http://hermes.roua.org/), [TeX4ht](http://tug.org/applications/tex4ht/mn.html), [LXir](http://www.lxir-latex.org/), [plasTeX](http://plastex.sourceforge.net/) and [sTeX](https://trac.kwarc.info/sTeX). I think it's cheaper to treat source files directly. And the conversion tools are not good in all times.

So the plan is to analyze the source code with regular expressions tools.