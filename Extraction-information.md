# Information to extract #

This is the information we want to extract.

## From the article ##

  * Source documents?: `article.tex`, `headers.tex`, etc. Frequently, the source is contained in one zipped file, say `foo.tar.gz` which contains several documents. Should we really record all the source documents? Or it's enough to record the URL in which we found the source?
  * URL in which we found the article. For example [arxiv:1406.3018](http://arxiv.org/e-print/1406.3018), [http://www.foo.com/bla.tex](http://www.foo.com/bla.tex)
  * Type of document: LaTeX, ConTeXt, PDF, etc. See [[Suported formats]]
  * Title
  * Authors
  * Date
  * Abstract
  * Propositions and definitions
  * References
  * License
  * Math Subject Classification
  * Keywords
  * Words: list of all different words
  * Language (probably, statistically infered)

## From the propositions ##

  * Statement
  * Hipothesis: the prerequisites. This is part of statement which is supposed
  * Thesis: the conclusion. This is part of statement
  * Name: if it has. For example `Theorem of Pythagoras`
  * References: if it links to references. For example `Theorem of Pythagoras [1, 2]` refers to `[1]` and `[2]` as references. `[1]` and `[2]` are links to documents. So we want to record this documents.
  * List of article whose belongs to. In which articles appear this proposition
  * Words (for quicker search). All words of proposition

# Documents for specific syntax #

  * [[LaTeX|latex:extraction]]