## Fields ##

For [[extracting|Extraction Information]] information from LaTeX documents syntax, we have to pick up:

### Mandatory ###

  * title (`\title`)
  * authors (`\author`). In some ocasions, there are more than one author and they put the institutions in which they belong to.
  * date
  * abstract (`\abstract` or `\begin{abstract} \end{abstract}`)
  * propositions and definitions: with amsthm: `\newtheorem{name}[...]{Theorem}` implies that `\begin{name}` starts a theorem. See amsthm documentation. See `\theoremstyle{definition}`
  * references: support \bibitems, support bibTeX and AMSRefs
  * mathematical subject classification
  * keywords (is there a command for that?)
  * words

### Optional ###

  * encoding of the text (`\inputenc`)

## Notes ##

  * We could collect the frequencies of the words (what are really words and not mathematical symbols?). The most frequent words are the most important? The key words are in the introduction and in the abstract.
  * In the definitions, the words inside `{\em }` are the terms we want

