This repository contains three parts:

  * The software which basically extracts information of several files. This information is mainly formed by propositions, definitions, axioms and any other textual information such as language, authors of paper, etc. The software is distributed under de the [Affero General Public License 3.0](http://www.gnu.org/licenses/agpl-3.0.html) or above.
  * The database of this extracted information. The database is distributed under the [Open Database License 1.0](http://opendatacommons.org/licenses/odbl/) or above.
  * And the documentation of the software, contents of the web page or any other things related. The documentation is released under the [Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/) or above.

All the contents of the repository are property of the respective owners: Xavier <quatrilio@gmail.com> and other contributions. All the contribution can be seen in the history of this repository or in the [[Contributors]] wiki page.