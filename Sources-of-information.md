# Sources of information #

We believe in free knowledge and free access to it. So we prioritize the information sources whose content are *free*. With this word we mean:
  1. The journal is part of [Open Access](http://doaj.org) or it has currency-free access
  1. The article is open sourced in any way
  1. The article has source code available

We collect information of the current state-of-art of Mathematics. So we just want to collect information about books, articles, surveys, ... *formal documents*. Presentations are *excluded*. We want a database which a researcher could use for referencing her or his work. So we need formal works.

## Source based documents ##

  * [ArXiV](http://www.arxiv.org). It serves a lot of LaTeX source documents. It has an [API](http://arxiv.org/help/robots) and a [bulk download procedure](http://arxiv.org/help/bulk_data). It has [RSS](http://arxiv.org/help/rss) which we could use for download only updated or new documents.

## Non-included ##

  * We *do not* include any information from Wikipedia. We strongly appreciate the effort and the aims of Wikipedia, but Wikipedia is a enciclopedia. So, their articles are refering to external sources by justifying their claims. This is exactly what we want: the external sources (articles, books, surveys, etc.). Also, Wikipedia article are changing continously, which is a complication. We want things which do not change continously, which are finished.


# References #

1. Directory of Open Access Journals. *[What is Open Access](http://doaj.org/oainfo)*
1. Edward M. Corrado. *[The Importance of Open Access, Open Source, and Open Standards for Libraries](http://www.istl.org/05-spring/article2.html)*
1. Wikipedia. *[Libre knowledge](https://en.wikipedia.org/wiki/Libre_knowledge)*
1. Wikipedia. *[Open Data](https://en.wikipedia.org/wiki/Open_data)*
1. Open Knowledge. *[Opendata](https://okfn.org/opendata/)*
