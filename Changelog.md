  1. Version 0.006
	* [Removing](https://github.com/quatrilio/propref/commit/6c06ff36272051a24ab8a4d9a3d64e104d013dcd) use of `--syntax='auto'` and `--encoding='auto'`
	* Use of `iconv-go` for convert encoding to UTF-8 after reading the file to analyze
	* Put the documentation (except `README.md`) in a Wiki
	* Add `--version` in `main.go`
	* Reorganize and rename the modules
	* Removing the test directory
  1. Version 0.005
	* Remove use of invmake and proper use of `go get`
	* Split library in directories: `fs` and `extractor`
	* Remove of `VERSION` file. Now it's integrated in `proprf` source file
  1. Version 0.004
	* Use of `godoc` capabilities in documenting code and bugs
  1. Version 0.003
	* Proper check of supported files
  1. Version 0.002
	* Preliminary version of `atype` module for determining the file type
	* Use of regular expressions for seeing if a file type is supported or not
  1. Version 0.001
	* Switch to [invmake](https://github.com/quatrilio/invmake)
	* Proper use of `package`
	* Each log prints the package from which it's generated.