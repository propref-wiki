## How to store documents? ##

  * We have to save document information with database backend, but the information format could be stored as xml, yaml, json, etc.
  * We have to choice a structured format
  * The database could be no-sql or sql. We have to analyse that

## How to link document? ##

  * RDF?
  * DOI?

## Crawl the web ##

  * Create a miniwebspider for crawl the web, find mathematical documents and "scan" their contents.
  * Support for RSS/Atom format for update database without spider use.

## Choose a programming language ##

  * The language should be:
    * Easy to program
    * Concurrent (in particular, fast).

Currently, we use Go Programming Language, but in future it could be ported to other programming language which fits this features (for example Rust)

## Interfaces ##

  * web
  * app for mobile?
  * cli?
  * console?

## Using libraries ##

  * NSQ could be useful for communication. When we have ready web server infrastructure.

## Integration with others resources ##

  * [Digital Object Identifier](https://en.wikipedia.org/wiki/Digital_object_identifier)
  * [CiteSeerX](http://citeseerx.ist.psu.edu) which has freely available [data](http://csxstatic.ist.psu.edu/about/data)
  * [Other](https://en.wikipedia.org/wiki/List_of_academic_databases_and_search_engines)