The aim of this project is to provide a search engine for (mathematical) axioms, definitions, theorems, lemmas, corollaries, etc. Not mere search engine of documents containing this elements, but a search engine of the results and axioms of Axiomatic Theories. An example: I want to know a new area of mathematics: triangular conorms.

  *  What are the main theorems of this area?
  *  What are the definitions I have to learn?
  *  What are the open problems?
  *  Can I get the demostrations of the main results?
  *  What sources I have to read first to understand this area?
  *  What are the most cited articles?
  *  Are there a books of this area?
  *  In what Axiomatic Theory does it belong to?
  *  What are the main authors in this area?
  *  And what are the journals of reference?

`PropRef` is created for *trying* to solving these problems. Perhaps we will not achieve it but we will try it.